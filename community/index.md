---
title: VenenuX Linux Community
---

# VenenuX Linux Community

Participation in the VenenuX community requires adherence to the [Code of Conduct](code-of-conduct.html).

<h2><i class="fa fa-hashtag" aria-hidden="true"></i>IRC</h2>

* [\#ispos](irc://irc.freenode.net/ispos) - For general discussion.

<h2><i class="fa fa-envelope" aria-hidden="true"></i>Mailing/Forum</h2>

* [VenenuX-users](mailto:vegnuli@googlegroups.com) -
  General user support.
  ([archive](https://groups.google.com/forum/m/#!forum/vegnuli)
  | [subscribe](mailto:vegnuli@googlegroups.com)
  | [unsubscribe](mailto:vegnuli+unsubscribe@googlegroups.com))
* [VenenuX-devel](mailto:venenuxsarisari@googlegroups.com) -
  VenenuX Linux development.
  ([archive](https://groups.google.com/forum/m/#!forum/venenuxsarisari)
  | [subscribe](mailto:venenuxsarisari@googlegroups.com)
  | [unsubscribe](mailto:venenuxsarisari+unsubscribe@googlegroups.com))

<h2><i class="fa fa-pencil" aria-hidden="true"></i>Wiki/Docs</h2>

Wiki has the documentation.

<https://venenux.github.io/venenux>

<h2><i class="fa fa-twitter" aria-hidden="true"></i>Twitter</h2>

<https://twitter.com/vegnuli>


vnx-mksite fork of alpine site
===========================

This project builds a default site from a template files.. 
was inspired by alpine-mksite (very poor documented) that 
has great performance (removing the media files and changed by gif files)

## What build for

Build s site by using markdown files as sources, 
then you can sync by git or rsync methods

## How to use

The following dependencies are needed to build the site:

  apk add make lua lua-feedparser lua-discount lua-yaml lua-lustache \
  	lua-filesystem

To build the site please run:

  make

To test as local web server please run:

  busybox httpd -p 8000 -h _out/

Then point your web browser to http://localhost:8000

